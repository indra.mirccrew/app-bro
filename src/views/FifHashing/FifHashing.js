import React from 'react'
import styles from './FifHashing.module.scss'
import { styled } from '@mui/material/styles'
import MainLayout from '../../layout/MainLayout/MainLayout'
import { ExcelRenderer } from 'react-excel-renderer'
import { useState } from 'react';
import md5 from 'blueimp-md5'
import { Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

const FifHashing = () => {
    const XLSX = require('xlsx')
    const fileSaver = require('file-saver')
    const [data, setData] = useState([])
    const [columnHeader, setColumnHeader] = useState([])
    const [downloadShown, setDownloadShown] = useState(false)
    const [fileName, setFileName] = useState('')

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: '#34A8D1',
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const handleChange = (e) => {
        if (e) {
            setDownloadShown(false)
            const file = e.target.files[0]
            setFileName(`${file.name.substr(0, file.name.lastIndexOf('.'))}-hashed`)

            ExcelRenderer(file, (err, res) => {
                if (err) {
                    console.log('Error: ', err)
                } else {
                    let array = []
                    for (let i = 1; i < res.rows.length; i++) {
                        let rowData = {}
                        rowData['NIK'] = res.rows[i][0]
                        rowData['Hashed'] = md5(md5(md5(res.rows[i][0])))
                        array.push(rowData)
                        rowData = {}
                    }
                    setColumnHeader(res.rows[0])
                    setData(array)
                    setDownloadShown(true)
                    handleSave(array, `${file.name.substr(0, file.name.lastIndexOf('.'))}-hashed`)
                }
            })
        }
    }

    const handleSave = (e, fileName) => {
        const ws = XLSX.utils.json_to_sheet(e)
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] }
        const excelBuffer = XLSX.write(wb, { bookType: 'csv', type: 'array' })
        return saveAsExcel(excelBuffer, fileName)
    }

    const saveAsExcel = (buffer, fileName) => {
        const data = new Blob([buffer], { type: 'csv' })
        return fileSaver.saveAs(data, fileName + '.csv')
    }

  return (
    <MainLayout>
        <div className={styles.container}>
            <div className={styles.row}>
                <input type="file" id='file' onChange={(e) => handleChange(e)} accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                <label htmlFor="file">
                    <CloudUploadIcon />
                    Upload
                </label>
                {downloadShown ? (
                    <button onClick={() => handleSave(data, fileName)}>Download</button>
                ) : null}
            </div>
            <div className={styles.row}>
                {downloadShown ? (
                    <TableContainer component={Paper}>
                        <Table aria-label='simple table'>
                            <TableHead>
                                <TableRow>
                                    {columnHeader.map((item, index) => <StyledTableCell key={index}>{item}</StyledTableCell>)}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((item, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{item.NIK}</TableCell>
                                        <TableCell>{item.Hashed}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                ) : null}
            </div>
        </div>
    </MainLayout>
  )
}

export default FifHashing