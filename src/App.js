import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import FifHashing from './views/FifHashing/FifHashing';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path='/' element={<FifHashing />} />
      </Routes>
    </Router>
  );
}

export default App;
