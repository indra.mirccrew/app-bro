import React, { useState } from 'react'
import styles from './MainSideNav.module.scss'
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';

const MainSideNav = () => {
  const [expandedMenu, setExpandedMenu] = useState(false)

  const handleExpandMenu = () => setExpandedMenu(!expandedMenu)

  return (
    <div className={styles.sidenav}>
      <div className={styles['sidenav-content']}>
        <div className={styles.menus}>
          <div className={styles.menu}>
            <div className={styles['menu-category']} onClick={handleExpandMenu}>
              <span>Module</span>
              <ExpandMoreRoundedIcon />
            </div>
            {expandedMenu ? (
              <div className={styles['menu-links']}>
                <ul>
                  <li>Hashing</li>
                </ul>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainSideNav