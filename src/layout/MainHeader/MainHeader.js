import styles from './MainHeader.module.scss'
import React from 'react'
import MautechLogo from '../../assets/images/mautech-logo.png'

const MainHeader = () => {
  return (
    <div className={styles.header}>
      <img src={MautechLogo} alt="Mautech" />
    </div>
  )
}

export default MainHeader