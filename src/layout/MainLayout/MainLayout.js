import React from 'react'
import styles from './MainLayout.module.scss'
import MainHeader from '../MainHeader/MainHeader'
import MainSideNav from '../MainSideNav/MainSideNav'

const MainLayout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <div className={styles['left-side']}>
        <MainSideNav />
      </div>
      <div className={styles['right-side']}>
        <MainHeader />
        {children}
      </div>
    </div>
  )
}

export default MainLayout