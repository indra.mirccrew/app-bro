FROM node:19-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install react-scripts --legacy-peer-deps

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
#CMD ["node", "-r", "./tracing.js", "index.js"]
